/*=========================================================================================
  File Name: main.js
  Description: main vue(js) file
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


import Vue from 'vue'
import App from './App.vue'

// Vuesax Component Framework
import Vuesax from 'vuesax'
import 'material-icons/iconfont/material-icons.css' //Material Icons
import 'vuesax/dist/vuesax.css'; // Vuesax
import VueSignaturePad from "vue-signature-pad";
import {CircularGaugePlugin } from "@syncfusion/ej2-vue-circulargauge";
Vue.use(CircularGaugePlugin);

Vue.use(Vuesax)
Vue.use(VueSignaturePad);

Vue.WS= Vue.prototype.WS = "https://webapichecklist.administraflotilla.com/api"; //"http://localhost:51104/api"; //
Vue.Api = Vue.prototype.Api = "https://webapiandroidtest.administraflotilla.com",
Vue.ApiTest = Vue.prototype.ApiTest = "https://webapifdt.administraflotilla.com";

Number.prototype.FormatCurrency = function () {
  return this.toLocaleString(
      'en-US', // leave undefined to use the browser's locale,
      // or use a string like 'en-US' to override it.
      { style: 'currency', currency: 'USD' }
  );
}

Number.prototype.FormatMiles = function () {
  
  return this.toLocaleString(
      'en-US', // leave undefined to use the browser's locale,
      // or use a string like 'en-US' to override it.
      { maximumSignificantDigits: 20 }
  );
}

Date.prototype.getWeek = function() {
  var onejan = new Date(this.getFullYear(),0,1);
  return Math.ceil((((this - onejan) / 86400000) + onejan.getDay()+1)/7);
}

Number.prototype.getFirstDayOfWeek = function(year, month) {  
  var week = this;
  var d = new Date("Jan 01, " + year + " 01:00:00");
  var w = d.getTime() + 604800000 * (week - 1);
  var date = new Date(w);
  var day = (date.getMonth() < month) ? new Date(year, month, 1).getDate() : date.getDate();
  return day;
}

Number.prototype.getLastDayOfWeek = function(year, month) {  
  var week = this;
  var d = new Date("Jan 01, " + year + " 01:00:00");
  var w = d.getTime() + 604800000 * (week - 1);
  var date = new Date(w + 518400000);
  var tmpDate = new Date(date.getFullYear(), date.getMonth(), 1);
  
  var day = new Date((date.getMonth() > month) ? tmpDate.setDate(tmpDate.getDate() - 1) : date);  
  return day.getDate();  
}


Number.prototype.Decimal = function () {
  return this.toLocaleString(
      'en-US', // leave undefined to use the browser's locale,
      // or use a string like 'en-US' to override it.
      { style: 'decimal', currency: 'USD' }
  );
}

// axios
import axios from "./axios.js"
Vue.prototype.$http = axios

// Theme Configurations
import '../themeConfig.js'


// Globally Registered Components
import './globalComponents.js'


// Styles: SCSS
import './assets/scss/main.scss'


// Tailwind
import '@/assets/css/main.css'


// Vue Router
import router from './router'


// Vuex Store
import store from './store/store'


// Vuejs - Vue wrapper for hammerjs
import { VueHammer } from 'vue2-hammer'
Vue.use(VueHammer)

import VeeValidate from 'vee-validate';
Vue.use(VeeValidate);

// PrismJS
import 'prismjs'
import 'prismjs/themes/prism-tomorrow.css'


// Feather font icon
require('./assets/css/iconfont.css')


Vue.config.productionTip = false

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')

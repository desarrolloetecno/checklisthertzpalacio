/*=========================================================================================
  File Name: sidebarItems.js
  Description: Sidebar Items list. Add / Remove menu items from here.
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


export default [
 
  // {
  //   url: "/checklist/Inspeccion",
  //   name: "G. H DE INSPECCIÓN",
  //   slug: "Inspeccion",
  //   icon: "FileIcon",
  // },
  // {
  //   url: "/checklist/Recepcion",
  //   name: "G. R DE TALLER",
  //   slug: "Recepcion",
  //   icon: "FileIcon",
  // },
  {
    url: null,
    name: "Checklist",
    tag: "2",
    tagColor: "warning",
    icon: "FileIcon",
    i18n: "Dashboard",
    submenu: [           
      {
        url: "/checklist/Inspeccion",
        name: "G. H DE INSPECCIÓN",
        slug: "Inspeccion",
        icon: "FileIcon",
      },         
      {
        url: "/checklist/Recepcion",
        name: "G. R DE TALLER",
        slug: "Recepcion",
        icon: "FileIcon",
      },   
    ]
  }
]
